\REM MIDI Player

This is broken down into five distinct phases: Data in, Data processing, Scheduling,
Output and General control (SWI's, memory etc.).

Each section is assembled into a separate module and then built into a single module.

(1)  Data In
     =======
     This can take place as a read from memory or as a read from an
     open file or as a read from a 'give data' function.
     If the open file is a stream coming in over a network then it
     may need to keep a look at the incoming file for more data that
     has not been received yet (i.e. a moveable Extent)

     Data Stream type
     ----------------

     dataStreamMemory%   = 1
     dataStreamFile%     = 2
     dataStreamOpenFile% = 3
     dataStreamGive%     = 4

     Functions
     ---------

     dataIn_Initialise
       Allows the dataIn to acknowledge a data stream type and request
       workspace.
       On entry:
          R0 = Data stream type%
       On exit:
          R0 = 1 for data stream type recognised, 0 for error
          R1 = Workspace required


     dataIn_Start
       This sets up a stream for input
       On entry:
          R0  = Data stream type%
          R1  = dataStreamMemory%   : Start of memory, Length in R2
                dataStreamFile%     : File name including path
                dataStreamOpenFile% : File handle
                dataStreamGive%     : Address of give routine, R12 in R2
          R2  = dataStreamMemory%   : Length of data
                dataStreamFile%     :
                dataStreamOpenFile% :
                dataStreamGive%     : Routine's R12
          R3  = Call back address
          R4  = Call back R12
          R12 = dataIn workspace pointer
        On exit:
          R0  = dataType
          Any errors?

     dataIn_Get
       This gets the specified bytes for input
       On entry:
         R0  = Function code
               : 0 = Get into registers from R0
               : 1 = Get into memory pointed to by R3
               : 2 = Return pointer to data in R0
         R1  = Offset
         R2  = Number of bytes required
         R3  = Memory pointer if required
         R12 = dataIn workspace pointer
       On exit:
         Depends on function code

     dataIn_Close
       Any open files are closed, any memory used is released
       On entry:
         R12 = dataIn workspace pointer
       On exit:



(2)  Data processing
     ===============
     Interpreting the incoming data into commands
     Turning the data into timed events and handling controls such
     as Start, Stop etc.

     Data type
     ---------
       dataType_MIDI% = 0


     Functions
     ---------

     dataProcess_Initialise
       Allows the data process to acknowledge the data type and request workspace
       On entry:
          R0  = Data type
       On exit:
          R0  = 1 Type recognised, 0 for error
          R1  = Workspace required

     dataProcess_Start
       Set up the bits required for playback
       On entry:
         R0  = Buffer pointer
         R1  = Buffer size

         R3  = Call back address
         R4  = Call back R12
         R12 = dataProcess workspace pointer


     dataProcess_Play
       Play the data from a given position
       On entry:
         R0  = Start position in time
         R1  = Loop
         R12 = dataProcess workspace pointer

     dataProcess_Stop
       Stop playback
         R0  = Function code
               : 0 = Stop
               : 1 = Pause
         R12 = dataProcess workspace pointer

     dataProcess_Position
       Re-position the play time
       On entry:
         R0  = +- value to jump by
         R12 = dataProcess workspace pointer


     dataProcess_Volume
       Change the overall volume
       On entry:
         R0 = New volume
         R12 = dataProcess workspace pointer


     dataProcess_Tempo
       Change overall tempo
       On entry:
         R0  = new tempo
         R12 = dataProcess workspace pointer


     dataProcess_Info
       On entry:
         R12 = dataProcess workspace pointer
       On exit:
         R0  = Length
         R1  = Current position
         R2  = Current volume
         R3  = Current tempo



(3)  Scheduler of the timed events
     =============================
     Time to the next event, at which point call the code address given
     Length of a tick
     The MIDI file provides timing specification as to the length of a
     tick and then uses these ticks as delta time values between events.
     The tempo can also change during playback

     Functions
     ---------
     Schedule_Initialise
       Allows the scheduler to request workspace.
       On entry:
          R0 = Data stream type%
       On exit:
          R0 = 1 for data stream type recognised, 0 for error
          R1 = Workspace required


     Schedule_StartClock
        Start a clock at a particular time resolution
        On entry:
           R0 = Time resolution for the clock
           R1 = 0 for new clock
              > 0 handle of old clock
           R2 = 0 Clear old events
              = 1 Keep old events
           R3  = Call back address
           R4  = Call back R12
           R12 = dataIn workspace pointer
        On exit:
           R0 = Handle of clock ( >0 )


     Schedule_StopClock
        Stop the clock
        On entry:
           R0 = Handle of clock
           R2 = 0 Clear old events
              = 1 Keep old events
           R12 = dataIn workspace pointer
        On exit:


     Schedule_PauseClock
        Pause the clock
        On entry:
           R0 = Handle of clock
           R12 = dataIn workspace pointer


     Schedule_ChangeClock
        Change the tempo of the clock
        On entry:
           R0 = Handle of the clock
           R1 = New tempo
           R12 = dataIn workspace pointer


     Schedule_SetEvent
        Schedule an event to happen at some time in the future
        On entry:
           R0 = Handle of the clock
           R1 = Delta Time for event
           R2 = Call back address
           R3 = Call back R12
           R12 = dataIn workspace pointer
        On exit:
           R2 = Handle for event


     Schedule_ChangeEvent
        Change an event's timing
        On entry:
           R0 = Handle of the clock
           R1 = New delta Time for event
           R2 = Handle for event
           R12 = dataIn workspace pointer
        On exit:



     Schedule_RemoveEvent
        Remove an event from the queue
        On entry:
           R0 = Handle of the clock
           R2 = Handle for event
           R12 = dataIn workspace pointer


(4)  Output
     ======
     Send a MIDI command to a driver/Synth etc.

     Output devices
     --------------
     Output_MIDI  = 0  (MIDI SWI's)
     Output_Synth = 1  (Direct calls into the Synth)

     Output data type
     ----------------
     OutputDataType_MIDI = 0


     Functions
     ---------
     Output_Initialise
       Allows the output to acknowledge the output data type and request workspace
       On entry:
          R0  = Output data type
       On exit:
          R0  = 1 Type recognised, 0 for error
          R1  = Workspace required

     Output_Start
       Set up the bits required for output
       On entry:
         R0  = Buffer pointer
         R1  = Buffer size

         R3  = Call back address
         R4  = Call back R12
         R12 = output workspace pointer

     Output_Send
       Send a command to the output stream
       On entry:
         R0.....  = Command
         R12 = output workspace pointer


(5)  A general management and SWI/Command interface
     This includes the management of multiple streams and handling memory etc.

     SWI's will include

     PlayFile
     PlayMemory
     PlayOpenFile
     PlayGive
     Start
     Stop
     Pause
     Continue
     Position
     Volume
     Tempo
     Info
     Options


     Call Backs
     ==========
     This section also provides general routines for use by the drivers.

     MemoryManager
     -------------

     Scheduler
     ---------
        GetCounter
        StartCounter
        ScheduleEvent
