MIDI Pattern Play
=================

This specification is for use with the module MIDIPlay to
provide easy playback of music data with facilities provided by the
MIDI play module that take away as much of the complication for
play handling as possible.

An application communicates with the module to control the patterns
that are to be played and the events that are contained in the
patterns.

Play handle
-----------
The first aspect of linking an application with the player is to
install a player handle for the application.  This is used to identify
the application to the player and to set up various bits of general
information.
This includes whether the output is to go via a MIDI SWI Port or via
an already defined MIDI Support driver.


   MIDIPlay_Player
      On entry:
         R0 = 0 for info, 1 to install, 2 to remove
         R1 = Player handle for info and remove
         R2 = Output destination
            = 0 for MIDI SWI's
            = MIDI Support handle number
      On exit:
         R0 = Player handle


Patterns
--------
Any number of patterns can be set up to play with the player.
Each pattern contains a header which is used to control items like the
instrument used, channel number, tempo, whether the pattern is looped etc.
Following the header is a list of events which can be changed or
added to at any time.

   MIDIPlay_Pattern
      Pattern installation
      On entry:
         R0 = Function code
                       0 = Info
                       1 = Install
                       2 = Remove
         R1 = Player handle
         R2 = Pattern handle
         R3 = Tempo
         R4 = Length
         R5 = MIDI Channel
         R6 = Instrument
      On exit:
         R0 = Pattern handle

   MIDIPlay_PatternParameter
      Read/write an overall pattern parameter
      On entry:
         R0 = Function code
                       0 = Read
                       1 = Write
         R1 = Player handle
         R2 = Pattern handle
         R3 = parameter number
         R4 = Value
      On exit:
         R0 = New value

   MIDIPlay_PatternPlay
      Play back control of pattern
      On entry:
         R0 = Function code
                       0 = Read status
                       1 = Play
                       2 = Stop
         R1 = Player handle
         R2 = Pattern handle
         R3 = Flags
                   Bit 0 = Loop
                       1 = Play reverse
         R4 = Loop count if bit 0 of flags set

Events
------
The body of the pattern is made up of events that can be in a variety of
formats.
They can be simple MIDI events such as Note on and Note off or more complex
events like 'play a sample' etc.


   MIDIPlay_PatternEvent
      Read/write a pattern event
      On entry:
         R0 = Function code
                       0 = Read
                       1 = Add
                       2 = Remove
                       3 = Change
         R1 = Player handle
         R2 = Pattern handle
         R3 = Event handle
         R4 = Value....
      On exit:
         R0 = Event handle
         R1 = Value
