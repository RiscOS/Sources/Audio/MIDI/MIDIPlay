;
; Copyright (c) 1997, Expressive Software Projects
; All rights reserved.
;
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:
;     * Redistributions of source code must retain the above copyright
;       notice, this list of conditions and the following disclaimer.
;     * Redistributions in binary form must reproduce the above copyright
;       notice, this list of conditions and the following disclaimer in the
;       documentation and/or other materials provided with the distribution.
;     * Neither the name of Expressive Software Projects nor the names of its
;       contributors may be used to endorse or promote products derived from
;       this software without specific prior written permission.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED. IN NO EVENT SHALL EXPRESSIVE SOFTWARE PROJECTS BE LIABLE FOR ANY
; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;

        B       Do_PatPlay_Initialise
        B       Do_PatPlay_Start
        B       Do_PatPlay_Player
        B       Do_PatPlay_Pattern
        B       Do_PatPlay_PatternParameter
        B       Do_PatPlay_PatternPlay
        B       Do_PatPlay_PatternEvent
        B       Do_PatPlay_Close

                           ^ 0,R12
work_PPCallBackTable       # 4
work_PlayerList            # 4
workPatPlaySize            * :INDEX:@

                           ^ 0
player_Handle              # 4
player_Next                # 4
player_Previous            # 4
player_Output              # 4
player_PatternList         # 4
playerWorkLen              * :INDEX:@

                           ^ 0
pattern_Handle             # 4
pattern_Next               # 4
pattern_Previous           # 4
pattern_Tempo              # 4
pattern_Length             # 4
pattern_Channel            # 4
pattern_Instrument         # 4
pattern_Flags              # 4
pattern_Transpose          # 4
pattern_SchedulerWorkspace # 4
pattern_OutputWorkspace    # 4
pattern_CallBackTable      # 4
pattern_EventList          # 4
pattern_Count              # 4
patternWorkLen             * :INDEX:@

pattern_FlagLoop           * 1

                           ^ 0
event_Handle               # 4
event_Next                 # 4
event_Previous             # 4
event_Value                # 4
event_Start                # 4
event_Length               # 4
event_Count                # 4
eventWorkLen               # :INDEX:@

                           ^ 0
CallBack_Module            # 4
CallBack_DataIn            # 4
CallBack_DataProcess       # 4
CallBack_Scheduler         # 4
CallBack_Output            # 4

        ; Get workspace
        ; On entry:
        ; On exit:
        ; R1 = Workspace required
Do_PatPlay_Initialise
        Push    "R2-R12,R14"

        LDR     R1,%FT10

        Pull    "R2-R12,PC"
10
        DCD     workPatPlaySize


        ; Set up the bits required for playback
        ; On entry:
        ; R3  = Call back address
        ; R4  = Call back R12
        ; R12 = dataProcess workspace pointer
Do_PatPlay_Start
        Push    "R1-R12,R14"

        STR     R3,work_PPCallBackTable

        MOV     R0,#0
        STR     R0,work_PlayerList

        Pull    "R1-R12,PC"

        ; Install a player
        ; On entry:
        ; R0 = Function code
        ;      0 = Info
        ;      1 = Install
        ;      2 = Remove
        ; R1 = Player handle for info and remove
        ; R2 = Output destination
        ; On exit:
        ; R0 = Player handle
Do_PatPlay_Player
        Push    "R2-R12,R14"

        CMP     R0,#0
        BEQ     PatPlay_PlayerInfo

        CMP     R0,#1
        BEQ     PatPlay_PlayerInstall

        CMP     R0,#2
        BEQ     PatPlay_PlayerRemove
        B       %FT10

PatPlay_PlayerInfo
        B       %FT10

PatPlay_PlayerInstall
        ; R2 = Output destination
        BL      makeNewPlayer

        ; R0 = Handle
        B       %FT10

PatPlay_PlayerRemove
10
        Pull    "R2-R12,PC"

        ; Pattern installation
        ; On entry:
        ; R0 = Function code
        ;      0 = Info
        ;      1 = Install
        ;      2 = Remove
        ; R1 = Player handle
        ; R2 = Pattern handle
        ; R3 = Tempo
        ; R4 = Length
        ; R5 = MIDI Channel,Instrument
        ; R6 = Transpose value
        ; On exit:
        ; R0 = Pattern handle
Do_PatPlay_Pattern
        Push    "R1-R12,R14"

        MOV     R7,R0                   ; Function code

        MOV     R0,R1
        BL      findPlayer              ; Get the player
        CMP     R0,#0
        BEQ     PatPlay_Pattern_PlayerNotFound

        MOV     R10,R0                  ; Pointer to player block

        CMP     R7,#0
        BEQ     PatPlay_PatternInfo

        CMP     R7,#1
        BEQ     PatPlay_PatternInstall

        CMP     R7,#2
        BEQ     PatPlay_PatternRemove
        B       %FT10

PatPlay_PatternInfo
        B       %FT10

PatPlay_PatternInstall
        BL      makeNewPattern
        B       %FT10

PatPlay_PatternRemove
10
        Pull    "R1-R12,PC"

PatPlay_Pattern_PlayerNotFound
        MOV     R0,#0

        Pull    "R1-R12,PC"

        ; Read/write an overall pattern parameter
        ; On entry:
        ; R0 = Function code
        ;      0 = Read
        ;      1 = Write
        ; R1 = Player handle
        ; R2 = Pattern handle
        ; R3 = parameter number
        ;      0 = Tempo, 1 = Length, 2 = Channel, 3 = Instrument, 4 = Transpose
        ; R4 = Value
        ; On exit:
        ; R0 = New value
Do_PatPlay_PatternParameter
        Push    "R1-R12,R14"

        MOV     R7,R0

        MOV     R0,R1
        BL      findPlayer              ; Get the player
        CMP     R0,#0
        BEQ     PatPlay_PatternPlay_PlayerNotFound

        MOV     R10,R0
        MOV     R0,R2
        BL      findPattern             ; Get the pattern
        CMP     R0,#0
        BEQ     PatPlay_PatternPlay_PatternNotFound

        MOV     R10,R0

        CMP     R3,#4
        BEQ     PatPlay_PatternParameter_Transpose
        B       %FT10

PatPlay_PatternParameter_Transpose
        CMP     R7,#0
        LDREQ   R0,[R10,#pattern_Transpose]
        STRNE   R4,[R10,#pattern_Transpose]
        MOVNE   R0,R4
        B       %FT10
10
        Pull    "R1-R12,PC"

        ; Play back control of pattern
        ; On entry:
        ; R0 = Function code
        ;      0 = Read status
        ;      1 = Play
        ;      2 = Stop
        ; R1 = Player handle
        ; R2 = Pattern handle
        ; R3 = Flags
        ; Bit 0 = Loop
        ; 1 = Play reverse
        ; R4 = Loop count if bit 0 of flags set
Do_PatPlay_PatternPlay
        Push    "R14"

        MOV     R7,R0

        MOV     R0,R1
        BL      findPlayer              ; Get the player
        CMP     R0,#0
        BEQ     PatPlay_PatternPlay_PlayerNotFound

        MOV     R10,R0
        MOV     R0,R2
        BL      findPattern             ; Get the pattern
        CMP     R0,#0
        BEQ     PatPlay_PatternPlay_PatternNotFound

        MOV     R10,R0

        CMP     R7,#0
        BEQ     PatPlay_PatternPlayStatus

        CMP     R7,#1
        BEQ     PatPlay_PatternPlayPlay

        CMP     R7,#2
        BEQ     PatPlay_PatternPlayStop
        B       %FT10

PatPlay_PatternPlayStatus
        BL      getClockTime
        B       %FT10

PatPlay_PatternPlayPlay
        STR     R3,[R10,#pattern_Flags]

        BL      tidyEvents
        BL      StartPPClock

        Push    "R12"
        MOV     R12,R10
        BL      CheckEvents
        Pull    "R12"
        B       %FT10

PatPlay_PatternPlayStop
        BL      StopClock
        BL      allPPNotesOff
10
        Pull    "PC"

PatPlay_PatternPlay_PlayerNotFound
PatPlay_PatternPlay_PatternNotFound
        Pull    "PC"

        ; Read/write a pattern event
        ; On entry:
        ; R0 = Function code
        ;      0 = Read
        ;      1 = Add
        ;      2 = Remove
        ;      3 = Change
        ; R1 = Player handle
        ; R2 = Pattern handle
        ; R3 = Event handle
        ; R4 = Value....
        ; R5 = Start time
        ; R6 = Length
        ; On exit:
        ; R0 = Event handle
        ; R1 = Value
Do_PatPlay_PatternEvent
        Push    "R2-R12,R14"

        MOV     R7,R0                   ; Function code

        MOV     R0,R1
        BL      findPlayer              ; Get the player
        CMP     R0,#0
        BEQ     PatPlay_PatternEvent_PlayerNotFound

        MOV     R10,R0
        MOV     R0,R2
        BL      findPattern             ; Get the pattern
        CMP     R0,#0
        BEQ     PatPlay_PatternEvent_PatternNotFound

        MOV     R10,R0

        ; R10 = Pattern pointer
        CMP     R7,#0
        BEQ     PatPlay_PatternEventRead

        CMP     R7,#1
        BEQ     PatPlay_PatternEventAdd

        CMP     R7,#2
        BEQ     PatPlay_PatternEventRemove

        CMP     R7,#3
        BEQ     PatPlay_PatternEventChange
        B       %FT10

PatPlay_PatternEventRead
        B       %FT10

PatPlay_PatternEventAdd
        BL      addEvent
        B       %FT10

PatPlay_PatternEventRemove
        B       %FT10

PatPlay_PatternEventChange
10
        Pull    "R2-R12,PC"

PatPlay_PatternEvent_PlayerNotFound
PatPlay_PatternEvent_PatternNotFound
        Pull    "R2-R12,PC"

Do_PatPlay_Close
        Push    "R14"
        LDR     R9,work_PlayerList
10
        CMP     R9,#0
        BEQ     %FT40

        LDR     R10,[R9,#player_PatternList]
20
        CMP     R10,#0
        BEQ     %FT30

        BL      closeScheduler
        LDR     R10,[R10,#pattern_Next]
        B       %BT20
30
        LDR     R9,[R9,#player_Next]
        B       %BT10
40
        Pull    "PC"

;;----------------------------------------------------------------------------
;; Memory management
;;----------------------------------------------------------------------------

        ; Get some memory
        ; On entry:
        ; R0 = Amount required
        ; On exit:
        ; R0 = Memory pointer
getPPMemory
        Push    "R1-R12,R14"

        ADD     R0,R0,#3                ; Word align
        BIC     R0,R0,#3

        MOV     R3,R0
        MOV     R0,#ModHandReason_Claim
        SWI     XOS_Module
        MOV     R0,R2

        Pull    "R1-R12,PC"

;;----------------------------------------------------------------------------
;; Event management
;;----------------------------------------------------------------------------

        ; Check the events in the list
        ; R12 = Pattern pointer
CheckEvents
        Push    "R0-R12,R14"

CheckEvents_Entry
        DebugRegIf Debug,"Check events = ",R12

        LDR     R9,[R12,#pattern_EventList]
        CMP     R9,#0
        BEQ     %FT90

        ; First of all find any events that have a 0 count
10
        LDR     R0,[R9,#event_Count]    ; 0 indicates do event now, bit 31 set if note active
        BIC     R0,R0,#1:SHL:31

        DebugRegIf Debug,"First Event count = ",R0

        CMP     R0,#0
        BEQ     CheckEvents_DoNow

CheckEvents_Next
        LDR     R9,[R9,#event_Next]
        CMP     R9,#0
        BNE     %BT10
        B       CheckEvents_SetCallBack

CheckEvents_DoNow
        LDRB    R0,[R9,#event_Value]
        CMP     R0,#1
        BEQ     CheckEvents_Note
        B       CheckEvents_Next

CheckEvents_Note
        LDR     R1,[R9,#event_Count]    ; 0 indicates note on, bit 31 set for note off
        CMP     R1,#0

        DebugRegIf Debug,"Event count = ",R1

        BNE     CheckEvents_NoteOff

        ; Note on
        LDR     R0,[R9,#event_Value]
        BIC     R0,R0,#&FF
        ORR     R0,R0,#&90
        LDR     R1,[R12,#pattern_Channel]
        ORR     R0,R0,R1
        LDR     R1,[R12,#pattern_Transpose]
        MOV     R2,R0,LSR #8
        AND     R2,R2,#&FF
        ADD     R2,R2,R1
        AND     R2,R2,#&FF
        BIC     R0,R0,#&FF00
        ORR     R0,R0,R2,LSL #8

        DebugRegIf Debug,"Do note on = ",R0

        ORR     R0,R0,#3:SHL:24

        Push    "R12"
        LDR     R1,[R12,#pattern_CallBackTable]
        LDR     R1,[R1,#CallBack_Output]
        LDR     R12,[R12,#pattern_OutputWorkspace]
        MOV     R14,PC
        ADD     PC,R1,#Output_Send
        Pull    "R12"

        LDR     R1,[R9,#event_Length]   ; Set count to end of note
        ORR     R1,R1,#1:SHL:31
        STR     R1,[R9,#event_Count]

        DebugRegIf Debug,"Set note count = ",R1

        B       CheckEvents_Next

CheckEvents_NoteOff
        LDR     R0,[R9,#event_Value]
        BIC     R0,R0,#&FF
        ORR     R0,R0,#&80
        LDR     R1,[R12,#pattern_Channel]
        ORR     R0,R0,R1
        LDR     R1,[R12,#pattern_Transpose]
        MOV     R2,R0,LSR #8
        AND     R2,R2,#&FF
        ADD     R2,R2,R1
        AND     R2,R2,#&FF
        BIC     R0,R0,#&FF00
        ORR     R0,R0,R2,LSL #8

        DebugRegIf Debug,"Do note off = ",R0

        ORR     R0,R0,#3:SHL:24

        Push    "R12"
        LDR     R1,[R12,#pattern_CallBackTable]
        LDR     R1,[R1,#CallBack_Output]
        LDR     R12,[R12,#pattern_OutputWorkspace]
        MOV     R14,PC
        ADD     PC,R1,#Output_Send
        Pull    "R12"

        MVN     R1,#0
        STR     R1,[R9,#event_Count]    ; Set value to -1 to indicate that note is over
        B       CheckEvents_Next

CheckEvents_SetCallBack
        LDR     R0,[R12,#pattern_Count] ; This indicates the end of the pattern

        DebugRegIf Debug,"Pattern count = ",R0

        CMP     R0,#0
        BEQ     CheckEvents_Stop

        ; Now go through the events to find the next one
        LDR     R9,[R12,#pattern_EventList]
        MVN     R8,#0
10
        ; Find the smallest callback value
        LDR     R0,[R9,#event_Count]

        TST     R0,#1:SHL:30
        BNE     CheckEvents_SmallestNext

        BIC     R0,R0,#1:SHL:31

        CMP     R8,#0                   ; First one
        MOVLT   R8,R0

        CMP     R0,R8
        MOVLT   R8,R0

CheckEvents_SmallestNext
        LDR     R9,[R9,#event_Next]
        CMP     R9,#0
        BNE     %BT10

        LDR     R0,[R12,#pattern_Count]

        CMP     R8,#0                   ; First one
        MOVLT   R8,R0

        CMP     R0,R8
        MOVLT   R0,R8

        DebugRegIf Debug,"Smallest call back time = ",R8

        ; Now R8 = Next callback value
        ; Adjust the count values for each event
        LDR     R9,[R12,#pattern_EventList]

CheckEvents_CountAdjustLoop
        LDR     R0,[R9,#event_Count]
        TST     R0,#1:SHL:30
        BNE     %FT10

        TST     R0,#1<<31               ; Set flag for later
        BIC     R0,R0,#1<<31            ; Remove
        SUB     R0,R0,R8                ; Adjust
        ORRNE   R0,R0,#1<<31            ; Replace if above flag set

        STR     R0,[R9,#event_Count]

        DebugRegIf Debug,"Adjust count to = ",R0
10
        LDR     R9,[R9,#event_Next]
        CMP     R9,#0
        BNE     CheckEvents_CountAdjustLoop

        LDR     R0,[R12,#pattern_Count]
        SUB     R0,R0,R8
        STR     R0,[R12,#pattern_Count]

        MOV     R1,R8
        ADR     R2,CheckEvents
        MOV     R3,R12
        BL      SetCallBack
90
        Pull    "R0-R12,PC"

CheckEvents_Stop
        MOV     R10,R12

        BL      StopClock
        BL      allPPNotesOff
        BL      tidyEvents

        ; Are we in loop mode or not?
        LDR     R0,[R12,#pattern_Flags]
        TST     R0,#pattern_FlagLoop
        BEQ     %BT90

        BL      StartPPClock
        B       CheckEvents_Entry

        Pull    "R0-R12,PC"

        ; Turn off any playing notes
        ; On entry:
        ; R10 = Pattern pointer
allPPNotesOff
        Push    "R0-R12,R14"

        Push    "R12"
        LDR     R1,[R10,#pattern_CallBackTable]
        LDR     R1,[R1,#CallBack_Output]
        LDR     R12,[R10,#pattern_OutputWorkspace]
        MOV     R14,PC
        ADD     PC,R1,#Output_AllNotesOff
        Pull    "R12"

        Pull    "R0-R12,PC"

        ; Make sure events are ready for start
        ; On entry:
        ; R10 = Pattern workspace
tidyEvents
        Push    "R0-R12,R14"

        LDR     R1,[R10,#pattern_Length]
        STR     R1,[R10,#pattern_Count]
        LDR     R9,[R10,#pattern_EventList]
10
        CMP     R9,#0
        BEQ     %FT20

        LDR     R1,[R9,#event_Start]
        STR     R1,[R9,#event_Count]

        LDR     R9,[R9,#event_Next]
        B       %BT10
20
        Pull    "R0-R12,PC"

;;----------------------------------------------------------------------------
;; Scheduler management
;;----------------------------------------------------------------------------

        ; Stop the scheduler clock
        ; R10 = Pattern workspace
StopClock
        Push    "R0-R12,R14"

        Push    "R12"
        LDR     R4,[R10,#pattern_CallBackTable]
        LDR     R4,[R4,#CallBack_Scheduler]
        LDR     R12,[R10,#pattern_SchedulerWorkspace]
        MOV     R14,PC
        ADD     PC,R4,#Scheduler_StopClock
        Pull    "R12"

        Pull    "R0-R12,PC"

        ; Start the scheduler clock
        ; R0 = Time resolution in 16.16 fraction
        ; R10 = Pattern workspace
StartPPClock
        Push    "R0-R12,R14"

        MOV     R1,#1:SHL:16

        Push    "R12"
        LDR     R4,[R10,#pattern_CallBackTable]
        LDR     R4,[R4,#CallBack_Scheduler]
        LDR     R12,[R10,#pattern_SchedulerWorkspace]
        MOV     R14,PC
        ADD     PC,R4,#Scheduler_ChangeClock
        Pull    "R12"

        Push    "R12"
        LDR     R1,[R10,#pattern_CallBackTable]
        LDR     R1,[R1,#CallBack_Scheduler]
        LDR     R12,[R10,#pattern_SchedulerWorkspace]
        MOV     R14,PC
        ADD     PC,R1,#Scheduler_StartClock
        Pull    "R12"

        Pull    "R0-R12,PC"

        ; Schedule a call back
        ; R1 = Delta time
        ; R2 = Call back address
        ; R3 = Call back R12
        ; R12 = Pattern workspace
SetCallBack

        Push    "R0-R12,R14"

        DebugRegIf Debug,"Set call back = ",R1
        DebugRegIf Debug,"Address to call = ",R2
        DebugRegIf Debug,"R12 = ",R3

        Push    "R12"
        LDR     R4,[R12,#pattern_CallBackTable]
        LDR     R4,[R4,#CallBack_Scheduler]
        LDR     R12,[R12,#pattern_SchedulerWorkspace]
        MOV     R14,PC
        ADD     PC,R4,#Scheduler_SetEvent
        Pull    "R12"

        Pull    "R0-R12,PC"

        ; Get the current scheduler pointer
        ; R10 = Pattern workspace
getClockTime
        Push    "R1-R12,R14"

        Push    "R12"
        LDR     R4,[R10,#pattern_CallBackTable]
        LDR     R4,[R4,#CallBack_Scheduler]
        LDR     R12,[R10,#pattern_SchedulerWorkspace]
        MOV     R14,PC
        ADD     PC,R4,#Scheduler_ReadClock
        Pull    "R12"

        Pull    "R1-R12,PC"

        ; Shut down any active schedulers
        ; On entry:
        ; R10 = Pattern workspace
closeScheduler
        Push    "R1-R12,R14"

        Push    "R12"
        LDR     R4,[R10,#pattern_CallBackTable]
        LDR     R4,[R4,#CallBack_Scheduler]
        LDR     R12,[R10,#pattern_SchedulerWorkspace]
        MOV     R14,PC
        ADD     PC,R4,#Scheduler_Quit
        Pull    "R12"

        Pull    "R1-R12,PC"

;;----------------------------------------------------------------------------
;; Player management
;;----------------------------------------------------------------------------

        ; Get player address from handle
        ; On entry:
        ; R0 = Handle
        ; On exit:
        ; R0 = Address or 0 for not found
findPlayer
        Push    "R1-R3,R14"

        LDR     R2,work_PlayerList
10
        CMP     R2,#0
        MOVEQ   R0,#0
        BEQ     %FT20

        LDR     R1,[R2,#player_Handle]  ; Found a player
        CMP     R0,R1                   ; Match handle
        MOVEQ   R0,R2
        BEQ     %FT20

        LDR     R2,[R2,#player_Next]    ; Or try the next one
        B       %BT10
20
        Pull    "R1-R3,PC"

        ; Get a unique handle for a player
getNewPlayerHandle
        Push    "R1,R14"

        MOV     R1,#1                   ; Try handles from 1 upwards
10
        MOV     R0,R1
        BL      findPlayer

        CMP     R0,#0                   ; If 0 then there is no player with that handle
        ADDNE   R1,R1,#1
        BNE     %BT10                   ; Otherwise try next number

        MOV     R0,R1

        Pull    "R1,PC"

        ; Create a new player with unique handle
        ; On entry:
        ; R2 = Output type
        ; On exit:
        ; R0 = Handle
makeNewPlayer
        Push    "R1-R12,R14"

        MOV     R0,#playerWorkLen
        BL      getPPMemory

        CMP     R0,#0                   ; Return handle of 0 if no memory available
        BEQ     %FT10

        ; Need to slot into the list
        MOV     R5,R0                   ; New memory pointer
        BL      getNewPlayerHandle

        ; R0 = Player handle
        LDR     R3,work_PlayerList
        CMP     R3,#0
        STREQ   R5,work_PlayerList
        BEQ     makeNewPlayer_Set

makeNewPlayer_Next
        LDR     R4,[R3,#player_Next]
        CMP     R4,#0

        ; If 0 then it is the last in the list
        MOVNE   R3,R4
        BNE     makeNewPlayer_Next

        STR     R5,[R3,#player_Next]

makeNewPlayer_Set
        STR     R3,[R5,#player_Previous]
        MOV     R3,#0
        STR     R3,[R5,#player_Next]
        STR     R0,[R5,#player_Handle]
        STR     R2,[R5,#player_Output]

        MOV     R3,#0
        STR     R3,[R5,#player_PatternList]
        ; Return handle in R0
10
        Pull    "R1-R12,PC"

;;----------------------------------------------------------------------------
;; Pattern management
;;----------------------------------------------------------------------------

        ; Get pattern address from handle
        ; On entry:
        ; R0  = Handle
        ; R10 = Player address
        ; On exit:
        ; R0  = Address or 0 for not found
findPattern
        Push    "R1-R3,R14"

        LDR     R2,[R10,#player_PatternList]
10
        CMP     R2,#0
        MOVEQ   R0,#0
        BEQ     %FT20

        LDR     R1,[R2,#pattern_Handle] ; Found a pattern
        CMP     R0,R1                   ; Match handle
        MOVEQ   R0,R2
        BEQ     %FT20

        LDR     R2,[R2,#pattern_Next]    ; Or try the next one
        B       %BT10
20
        Pull    "R1-R3,PC"

        ; Get a unique handle for a pattern
getNewPatternHandle
        Push    "R1,R14"

        MOV     R1,#1                   ; Try handles from 1 upwards
10
        MOV     R0,R1
        BL      findPattern

        CMP     R0,#0                   ; If 0 then there is no pattern with that handle
        ADDNE   R1,R1,#1
        BNE     %BT10                   ; Otherwise try next number

        MOV     R0,R1

        Pull    "R1,PC"

        ; Make a new pattern with unique handle id
        ; On entry:
        ; R3  = Tempo
        ; R4  = Length
        ; R5  = MIDI Channel,Instrument
        ; R6  = Transpose
        ; R10 = Player memory pointer
        ; On exit:
        ; R0 = Pattern handle
makeNewPattern
        Push    "R1-R12,R14"

        MOV     R0,#patternWorkLen
        BL      getPPMemory

        CMP     R0,#0
        BEQ     %FT90

        MOV     R7,R0                   ; Pointer to new pattern block

        BL      getNewPatternHandle

        LDR     R2,[R10,#player_PatternList]
        CMP     R2,#0

        STREQ   R7,[R10,#player_PatternList]
        BEQ     makeNewPattern_Set
10
        LDR     R8,[R2,#pattern_Next]
        CMP     R8,#0

        ; If 0 then it is the last in the list

        MOVNE   R2,R8
        BNE     %BT10

        STR     R7,[R2,#pattern_Next]

makeNewPattern_Set
        STR     R2,[R7,#pattern_Previous]
        MOV     R2,#0
        STR     R2,[R7,#pattern_Next]
        STR     R0,[R7,#pattern_Handle]
        MOV     R9,R0

        STR     R3,[R7,#pattern_Tempo]
        STR     R4,[R7,#pattern_Length]
        STR     R5,[R7,#pattern_Channel]
        STR     R6,[R7,#pattern_Transpose]

        MOV     R2,#0
        STR     R2,[R7,#pattern_EventList]

        LDR     R8,[R10,#player_Output] ; MIDI Support Driver number

        MOV     R10,R7                  ; pattern block

        LDR     R1,work_PPCallBackTable
        STR     R1,[R10,#pattern_CallBackTable]

        ; Get a scheduler and an output driver
        LDR     R1,work_PPCallBackTable
        LDR     R1,[R1,#CallBack_Scheduler]
        ; No R12 yet
        MOV     R14,PC
        ADD     PC,R1,#Scheduler_Initialise

        MOV     R0,R1
        BL      getPPMemory
        STR     R0,[R10,#pattern_SchedulerWorkspace]

        Push    "R12"
        LDR     R1,work_PPCallBackTable
        LDR     R1,[R1,#CallBack_Scheduler]
        LDR     R12,[R10,#pattern_SchedulerWorkspace]
        MOV     R14,PC
        ADD     PC,R1,#Scheduler_Start
        Pull    "R12"

        LDR     R1,work_PPCallBackTable
        LDR     R1,[R1,#CallBack_Output]
        ; No R12 yet
        MOV     R14,PC
        ADD     PC,R1,#Output_Initialise

        MOV     R0,R1
        BL      getPPMemory
        STR     R0,[R10,#pattern_OutputWorkspace]

        Push    "R12"
        MOV     R0,#1                   ; Output via MIDI Support
        MOV     R1,R8                   ; Driver number
        LDR     R2,work_PPCallBackTable
        LDR     R2,[R2,#CallBack_Output]
        LDR     R12,[R10,#pattern_OutputWorkspace]
        MOV     R14,PC
        ADD     PC,R2,#Output_Start
        Pull    "R12"

        MOV     R0,R9                   ; Handle
90
        Pull    "R1-R12,PC"

;;----------------------------------------------------------------------------
;; Event management
;;----------------------------------------------------------------------------

        ; Get event address from handle
        ; On entry:
        ; R0  = Handle
        ; R10 = Pattern address
        ; On exit:
        ; R0  = Address or 0 for not found
findEvent
        Push    "R1-R3,R14"

        LDR     R2,[R10,#pattern_EventList]
10
        CMP     R2,#0
        MOVEQ   R0,#0
        BEQ     %FT20

        LDR     R1,[R2,#event_Handle]   ; Found a pattern
        CMP     R0,R1                   ; Match handle
        MOVEQ   R0,R2
        BEQ     %FT20

        LDR     R2,[R2,#event_Next]     ; Or try the next one
        B       %BT10
20
        Pull    "R1-R3,PC"

        ; Get a unique handle for a pattern
getNewEventHandle
        Push    "R1,R14"

        MOV     R1,#1                   ; Try handles from 1 upwards
10
        MOV     R0,R1
        BL      findEvent

        CMP     R0,#0                   ; If 0 then there is no pattern with that handle
        ADDNE   R1,R1,#1
        BNE     %BT10                   ; Otherwise try next number

        MOV     R0,R1

        Pull    "R1,PC"

        ; Add an event to a pattern
        ; On entry:
        ; R4  = Value....
        ; R5  = Start time
        ; R6  = Length
        ; R10 = Pattern memory pointer
        ; On exit:
        ; R0 = Event handle
addEvent
        Push    "R1-R12,R14"

        MOV     R0,#eventWorkLen
        BL      getPPMemory

        CMP     R0,#0
        BEQ     %FT20

        MOV     R7,R0                   ; Pointer to new event block

        BL      getNewEventHandle

        LDR     R2,[R10,#pattern_EventList]
        CMP     R2,#0

        STREQ   R7,[R10,#pattern_EventList]
        BEQ     addEvent_Set
10
        LDR     R8,[R2,#event_Next]
        CMP     R8,#0

        ; If 0 then it is the last in the list
        MOVNE   R2,R8
        BNE     %BT10

        STR     R7,[R2,#event_Next]

addEvent_Set
        STR     R2,[R7,#event_Previous]
        MOV     R2,#0
        STR     R2,[R7,#event_Next]
        STR     R0,[R7,#event_Handle]

        STR     R4,[R7,#event_Value]

        DebugRegIf Debug,"Event start = ",R5
        DebugRegIf Debug,"Event length = ",R6

        CMP     R5,#0
        BGE     addEvent_TimeOK

        BL      getClockTime
        MOV     R5,R0

addEvent_TimeOK
        STR     R5,[R7,#event_Start]
        STR     R6,[R7,#event_Length]
        MOV     R2,#0
        STR     R2,[R7,#event_Count]
20
        Pull    "R1-R12,PC"

        END
